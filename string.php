<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php

        /*
            SOAL NO 1
            Tunjukan dengan menggunakan echo berapa panjang dari string yang diberikan berikut! Tunjukkan juga jumlah kata di dalam kalimat tersebut!

            Contoh:
            $string = "PHP is never old";
            Output:
            Panjang string: 16,
            Jumlah kata: 4
        */

        echo "<h3> Soal No 1 :</h3>";
        $KataDasar  = "Hallo, I Am Programmer"; // ini string nya
        $SoalSatu = ""; // buat variable biar rapi dan tinggal echo di bawah
        $SoalSatu .= "<ul>";
        $SoalSatu .= "<li>String : ".$KataDasar."</li>"; // isi string nya
        $SoalSatu .= "<li>Panjang String : ".strlen($KataDasar)."</li>"; // strlen adalah untuk cek panjang string
        $SoalSatu .= "<li>Jumlah Kata : ".str_word_count($KataDasar)."</li>"; // str_word_count adalah untuk menjumlahkan kata di dalam string
        $SoalSatu .= "</ul>";

        // ini fungsi yang sama ama di atas
        $first_sentence = "Hello PHP!" ; // Panjang string 10, jumlah kata: 2
        $SoalSatu .= "<ul>";
        $SoalSatu .= "<li>String : ".$first_sentence."</li>";
        $SoalSatu .= "<li>Panjang String : ".strlen($first_sentence)."</li>";
        $SoalSatu .= "<li>Jumlah Kata : ".str_word_count($first_sentence)."</li>";
        $SoalSatu .= "</ul>";

        // ini fungsi yang sama ama di atas
        $second_sentence = "I'm ready for the challenges"; // Panjang string: 28,  jumlah kata: 5
        $SoalSatu .= "<ul>";
        $SoalSatu .= "<li>String : ".$second_sentence."</li>";
        $SoalSatu .= "<li>Panjang String : ".strlen($second_sentence)."</li>";
        $SoalSatu .= "<li>Jumlah Kata : ".str_word_count($second_sentence)."</li>";
        $SoalSatu .= "</ul>";

        echo $SoalSatu;

        echo "<h3> Soal No 2</h3>";
        /*
            SOAL NO 2
            Mengambil kata pada string dan karakter-karakter yang ada di dalamnya.
        */
        $string2 = "I love PHP"; // ini string nya

        echo "<label>String: </label> \"$string2\" <br>";
        // ------------------------------------------------------
        echo "Kata pertama: " . substr($string2, 0, 1) . "<br>" ;
        // ini menampilkan string yang di inginkan, misal : substr($string2, 0, 1) , 0 adalah posisi karakter dalam text, di awalai dengan 0 bukan 1, 1 adalah jumlah karakter yang ingin di tampilkan
        // ------------------------------------------------------
        // Lanjutkan di bawah ini
        echo "Kata kedua: ". substr($string2, 2, 4) . "<br>" ;   // substr($string2, 2, 4) , 2 adalah posisi karakter dalam text,di awalai dengan 0 bukan 1, 4 adalah jumlah karakter yang ingin di tampilkan
        echo "Kata Ketiga: ". substr($string2, 7, 3)  ; // ini sama aja

        echo "<h3> Soal No 3 </h3>";
        /*
            SOAL NO 3
            Mengubah karakter atau kata yang ada di dalam sebuah string.
        */
        $string3 = "PHP is old but sexy!";
        // OUTPUT : "PHP is old but awesome"

        echo "String: \"$string3\" ";
        $text_replace = str_replace("sexy!","beauty",$string3); // str_replace(find, replace, string); sama aja kaya gini : str_replace(kata_yang_dicari,ganti_ke_kata_lain,asal_kata_yang_mau_di_eksekusi)
        echo "<br>".$text_replace;

    ?>
</body>
</html>
